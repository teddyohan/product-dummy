package com.dummy.product.dao;

import com.dummy.product.model.Product;
import com.dummy.product.model.Products;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Repository
@Component
public class ProductClient {

    @Value("${api.host.baseurl}")
    private String apiHost;

    private RestTemplate restTemplate = new RestTemplate();

    public List<Product> getProducts() {
        final ResponseEntity<String> response = restTemplate.exchange(
                apiHost,
                HttpMethod.GET, null, String.class
        );
        final ResponseEntity<Products> responsex = restTemplate.exchange(
                apiHost,
                HttpMethod.GET, null, Products.class
        );
        if (responsex.getStatusCode().equals(HttpStatus.OK)) {
            return Arrays.asList(responsex.getBody().getData());
        }
        return null;
    }

    public void addProduct(Product product){
        System.out.println(product.getName());

    }
}
