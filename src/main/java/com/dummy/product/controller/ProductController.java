package com.dummy.product.controller;

import com.dummy.product.model.Product;
import com.dummy.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
//@Slf4j
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getProducts (){
        List<Product> ret = null;
        try {
            ret= productService.getProducts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<List<Product>>(ret, HttpStatus.CREATED);

    }
    @GetMapping("/add-products")
    public ResponseEntity<List<Product>> addProducts (){
        List<Product> ret = null;
        try {
            ret= productService.addProduct();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<List<Product>>(ret, HttpStatus.CREATED);

    }
}
