package com.dummy.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Products {
    private Integer page;

    @JsonProperty(value = "per_page")
    private Integer perPage;
    private Integer total;
    @JsonProperty(value = "total_pages")
    private Integer totalPages;
    private Product data[];

    public Product[] getData() {
        return data;
    }
    public void setData(Product[] data) {
        this.data = data;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
