package com.dummy.product.service;

import com.dummy.product.dao.ProductClient;
import com.dummy.product.dao.ProductRepository;
import com.dummy.product.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductClient productDao;

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getProducts(){
        return productDao.getProducts();
    }
    public List<Product> addProduct(){
        List<Product> result= new ArrayList<>();
        List<Product> products= productDao.getProducts();
        if(products!=null &&products.size()>0){
            products.forEach(product->{
                if (product.getName().contains("a")){
                    productRepository.save(product);
                    result.add(product);
                }
            });
        }
       return result;
    }
}
